<?php

use App\Http\Controllers\DashboardController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () { return view('welcome');});
Route::get('/', function () { return view('dashboard', ["title" => "Dashboard"]);});
Route::get('/test', function () { return view('test');});
Route::get('/user', function () { return view('user', ["title" => "User"]);});
Route::get('/userlist', function () { return view('userlist', ["title" => "User List"]);});
Route::get('/about', function () { return view('about', ["title" => "About"]);});

